import { Component, ElementRef, ViewChild } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { Geolocation } from '@ionic-native/geolocation';
import firebase from 'firebase';
declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  limitador: number = 0;
  markers: Array<any> = [];

  parquimetro = { distancia: '', disponibilidad: '', moneda: '' }
  parquimetro_fire: any;
  parquimetros: any;
  constructor(
    public navCtrl: NavController,
    public database: AngularFireDatabase,
    public alrtCtrl: AlertController,
    public geolocation: Geolocation
  ) {
    this.parquimetros = firebase.database().ref('/parquimetros');
    this.limitador = 0;
  }

  ionViewDidLoad() {
    this.loadMap();

  }

  loadMap() {

    this.geolocation.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.buscarEstacionamiento();
    }, (err) => {
      console.log(err);
    });


  }

  buscarEstacionamiento() {
    this.parquimetros.orderByChild('titulo').on('value', parquimetrosList => {
      for (let index = 0; index < this.markers.length; index++) {
        let marcador = this.markers[index];
        marcador.setMap(null);
      }
      parquimetrosList.forEach(data => {
        let parquimetro = data.val();
        let parquimetro_posicion = new google.maps.LatLng({ lat: parquimetro.lat, lng: parquimetro.lng });
        let icono = (parquimetro.disponible == "ocupado") ? "http://maps.google.com/mapfiles/ms/icons/red-dot.png" : "http://maps.google.com/mapfiles/ms/icons/green-dot.png"
        let marker = new google.maps.Marker({
          map: this.map,
          position: parquimetro_posicion,
          icon: icono
        });
        let content = '<h5>' + parquimetro.hora + "h " + parquimetro.minutos + "m " + parquimetro.segundos + "s "+ '</h5>';

        setTimeout(() => {
            this.addInfoWindow(marker, content);
        }, 100);
        
        this.limitador += 1;
        this.markers.push(marker);

      })
    })
  }

  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    infoWindow.open(this.map, marker);


  }

}
